require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get habitaciones" do
    get :habitaciones
    assert_response :success
  end

  test "should get contactanos" do
    get :contactanos
    assert_response :success
  end

end
